# Task for a company interview 
Here you will find the API documentation for the REST API: https://v2.developer.pagerduty.com/docs 

And here is the API reference: https://api-reference.pagerduty.com

And this is the API Key for authentication: 41k-DnZszhjiety_6NTZ

Please try to complete the following tasks:
1. Setup a console program.
2. Enable the user to provide a year and a month as command line parameters.
3. Fetch the list of incidents from the PagerDuty API which have been occurred in the month provided by the user. It might be required to use paging to get all events for the month.
4. Make sure to apply the correct time zone, which is CET in our case. The API has support for time zones.
5. Extract the following fields from the retrieved incidents:
    1. incident_number 
    2. title  
    3. status 
    4.created_at 
    5.urgency    
Write the above fields for all events for the specific month in a CSV file. Please name the file according to this pattern: PD_Incidents_<year>_<month>.csv

After that make a monthly overtime report as well.